*En desarrollo

Cuevano es una aplicacion web desarrollada con el fin de simular un videoclub online. Todo esto con fines de familiarizarme con ASP.NET MVC

El proyecto hasta la fecha cuenta con:
- -Arquitectura MVC
- -CRUD
- -Forms con validaciones en server y cliente.
- -Servicio RESTful usando ASP.NET Web API
- -Uso de jQuery 
- -Autenticación y autorización
- -Uso de Entity Framework para querys o actualizar datos
- -Uso de Automapper


