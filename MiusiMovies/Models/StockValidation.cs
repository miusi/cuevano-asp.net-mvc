﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace MiusiMovies.Models
{
    public class StockValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var movie = (Movie) validationContext.ObjectInstance;
            
            return (movie.Stock > 0 && movie.Stock < 21) ? ValidationResult.Success : new ValidationResult("Stock must be greater than 0 and less than 20");

        }
    }
}