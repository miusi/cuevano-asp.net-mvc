namespace MiusiMovies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameFieldToMembershipType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MembershipTypes", "Name", c => c.String());
            Sql("UPDATE MembershipTypes SET name = 'Free' WHERE id = 1;");
            Sql("UPDATE MembershipTypes SET name = 'Monthly' WHERE id = 2;");
            Sql("UPDATE MembershipTypes SET name = '3 Months' WHERE id = 3;");
            Sql("UPDATE MembershipTypes SET name = 'Annually' WHERE id = 4;");
        }
        
        public override void Down()
        {
            DropColumn("dbo.MembershipTypes", "Name");
        }
    }
}
