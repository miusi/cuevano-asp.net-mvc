namespace MiusiMovies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FillUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7748ab9d-ef61-4ed8-939f-8d84f78fdc5f', N'admin@miusimovies.com', 0, N'AB5UT2cP6gSIMu2jqMoNO502VKzT6EZGSJxwMR0E67pYmNTyehXyxsryxCD0jJB8Fg==', N'6262ef2d-b6f8-4c74-b1ee-cee30268535e', NULL, 0, 0, NULL, 1, 0, N'admin@miusimovies.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7f442c6f-6636-459c-8df4-b43a6ee56cf9', N'guest@guest.com', 0, N'AInZAnR+kLMfL9Q+vjCr3Rd0O8TLjGHXLaxqKz+KUtuqBUK/55dsPryCegA/xw0Khg==', N'52db8594-758e-482d-9f82-e8b4c178f5a0', NULL, 0, 0, NULL, 1, 0, N'guest@guest.com')

                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'004ee2c9-c7e0-41ad-bd25-e68ec4e427a6', N'CanManageMovie')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7748ab9d-ef61-4ed8-939f-8d84f78fdc5f', N'004ee2c9-c7e0-41ad-bd25-e68ec4e427a6')
                ");
        }
        
        public override void Down()
        {
        }
    }
}
