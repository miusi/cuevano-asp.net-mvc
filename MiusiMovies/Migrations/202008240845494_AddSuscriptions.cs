namespace MiusiMovies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSuscriptions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "IsSuscribedToNewsletter", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "IsSuscribedToNewsletter");
        }
    }
}
