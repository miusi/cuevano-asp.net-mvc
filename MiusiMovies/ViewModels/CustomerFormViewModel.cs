﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MiusiMovies.Models;

namespace MiusiMovies.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Customer Customer { get; set; }
    }
}