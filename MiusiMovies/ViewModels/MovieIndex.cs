﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MiusiMovies.Models;

namespace MiusiMovies.ViewModels
{
    public class MovieIndex
    {
        public List<Movie> MoviesList{ get; set; }
    }
}