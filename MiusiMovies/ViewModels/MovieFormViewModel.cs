﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MiusiMovies.Models;

namespace MiusiMovies.ViewModels
{
    public class MovieFormViewModel
    {
        public IEnumerable<Genre> GenreList { get; set; }

        public int? Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        
        [Display(Name = "Genre")]
        [Required]
        public byte? GenreId { get; set; }

        [Display(Name = "Release Date (MM/dd/yyyy)")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? ReleaseDate { get; set; }
        
        [Required]
        [Display(Name = "Number in Stock")]
        [StockValidation]
        public int? Stock { get; set; }
        
        public string Title
        {
            get
            {
                return Id != 0 ? "New Movie" : "Edit Movie";
            }
        }

        public MovieFormViewModel(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            ReleaseDate = movie.ReleaseDate;
            Stock = movie.Stock;
            GenreId = movie.GenreId;
        }

        public MovieFormViewModel()
        {
            Id = 0;
        }
    }
}