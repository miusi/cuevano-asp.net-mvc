﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MiusiMovies.Startup))]
namespace MiusiMovies
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
